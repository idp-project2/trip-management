const router = require('express').Router();
let User = require('../models/user.model');

router.route('/').get((req, res) => {
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err));
});
// register new user
router.route('/register').post((req, res) => {
  // take info from the request body
    var username = req.body.username;
    var name = req.body.name;
    var surName = req.body.surName;
    var driver = req.body.driver;
    var email = req.body.email;
    var password = req.body.password;

    // make new Document
    var newUser = {
        "username": username,
        "name": name,
        "surName":surName,
        "driver":driver,
        "email": email,
        "password": password,
        "pendingForm": "",
        "activeRoute": ""};
      
    User.bulkWrite([
        {insertOne: {document: newUser}}])
      .then(user => res.json(user))
      .catch(err => res.status(400).json('Error: ' + err));
});

// get user by username
router.route('/user/:username').get((req, res) => {
  var username = req.params.username;
  var query = {"username": username};
  
  User.find(query)
      .then(users => res.json(users))
      .catch(err => res.status(400).json('Error: ' + err));
});
// update pending form
router.route('/updateForm/:username').post((req, res) => {
  var username = req.params.username;
  var formId = req.body.formId;
  var userQuery = {"username": username};
  var toUpdate = {$set: {"pendingForm" : formId}};
    
  User.updateOne(userQuery, toUpdate)
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});
// update active route for one user
router.route('/updateRoute/:username').post((req, res) => {
  var username = req.params.username;
  var routeId = req.body.routeId;

  var toUpdate = {$set : {  activeRoute : routeId,
                            pendingForm : ""}};
  var userQuery = {"username": username};
  User.updateOne(userQuery, toUpdate)
      .then(users => res.json(users))
      .catch(err => res.status(400).json('Error: ' + err));
});
module.exports = router;
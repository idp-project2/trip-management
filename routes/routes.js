const router = require('express').Router();
let Route = require('../models/route.model');
let User = require('../models/user.model');

router.route('/').get((req, res) => {
    Route.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err));
});
// register a new route -> teoretic nici n-am nevoied e asta fml
router.route('/addRoute').post((req, res) => {
  // TODO: patch this thingy
  // take info from the request body
    var startPoint = req.body.startPoint;
    var finishPoint = req.body.finishPoint;
    var distance = req.body.distance;
    // id of the user driver
    var driver = req.body.driver;
    // id of the user passenger
    var passenger = req.body.passenger;

    // make new Document
    var newRoute = {
        "startPoint": startPoint,
        "finishPoint" : finishPoint,
        "driver":driver,
        "distance": distance,
        "passenger": passenger
    };

    Route.bulkWrite([
        {
          insertOne: {
            document: newRoute
          }
        }
      ])
      .then(route => res.json(route))
      .catch(err => res.status(400).json('Error: ' + err));

});
 
/*
  finish a route -> just the driver
  primesc ca argument id-ul rutei
  trebuie sters si din user campul de active route :)
*/
router.route('/deleteRoute').delete((req, res) => {
  var id = req.body.id;
  var query = {"_id" : id};
  let driverUsername = "";
  let passengerUsername = "";

  // iau numele participantilor
  Route.find(query)
      .then(route => function() {
          driverUsername = route.driver;
          passengerUsername = route.passenger;
          console.log("Got the usernames");
      })
      .catch(function(err){console.log(err)});

  // stergem uta
  Route.deleteOne(query)
      .then(function(){console.log("Deleted the route");})
      .catch(function(error){console.log(error);});
  
  // actualizam ruta activa a userilor
  var queryDriver = {"username" : driverUsername};
  var queryPassenger = {"username": passengerUsername};
  var toUpdate = {$set: {activeRoute: ""}};

  User.updateOne(queryPassenger, toUpdate)
  .then(function(){console.log("Update the passenger's active route");})
  .catch(function(error) {console.log(error);});
  User.updateOne(queryDriver, toUpdate)
  .then(function(){console.log("Update the driver's active route");})
  .catch(function(error) {console.log(error);});

});

/* 
  get a route by id
*/
router.route('/getRouteById').delete((req, res) => {
  var id = req.body.id;
  var query = {"_id": id};

  Route.findOne(query)
      .then(route => res.json(route))
      .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;
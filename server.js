const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

// momentan o sa tin baza de date in atlas, lmk if something does not work
// e mai usor de vizualizat asa pentru inceput, daca nu merge conectarea, probabuil trebuie sa-ti adaug

const uri = 'mongodb+srv://ewoana:huehue@cluster0.p9ylj.mongodb.net/PWEB?retryWrites=true&w=majority'
// const uri = 'mongodb://mongo-db:27017/PWEB'

mongoose.connect(uri, {useNewUrlParser: true, useUnifiedTopology: true}).catch(err => console.log(err) );
const connection = mongoose.connection;
connection.once('open', () => {
     console.log("MongoDB database connection established succsefully");
 });

const userRouter = require('./routes/users');
app.use('/users', userRouter); 
const routeRouter = require('./routes/routes');
app.use('/routes', routeRouter);
const formRouter = require('./routes/forms');
app.use('/forms', formRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});



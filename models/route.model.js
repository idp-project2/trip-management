const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// nu sunt sigura daca sa tin vector de rute active sau doar cate una per person
const routeSchema = new Schema({
    startPoint: {
        type: String
    },
    finishPoint: {
        type: String
    },
    driver: {  // username
        type: String
    },
    passenger: { //username
        type: String
    }
    // am nevoie sa am status daca e finished ruta sau nu??
});

const Route = mongoose.model('Route', routeSchema);
module.exports = Route;

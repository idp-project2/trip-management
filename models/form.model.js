const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// nu sunt sigura daca sa tin vector de rute active sau doar cate una per person
const formSchema = new Schema({
    startPoint: {
        type: String
    },
    finishPoint: {
        type: String
    },
    distance: {
        type: Number
    },
    passenger: {   // username
        type: String
    },
    numberPeople: {
        type: Number
    },
    maxDate: {
        type: Date
    },
    driverAccept: {
        type: Boolean
    },
    drivers: {  // array de driver usernames
        type: [String],
        'default': [] 
    }
});

const Form = mongoose.model('Form', formSchema);
module.exports = Form;

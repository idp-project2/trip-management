const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        trim: true,
        unique: true
    },
    // auth0 thingy?
    password: {
        type: String
    },
    name: {
        type: String,
        trim: true
    },
    surName: {
        type: String
    },
    driver: {
        type: Boolean
    },
    email: {
        type: String
    },
    activeRoute: { // primeste id-ul rutei activa
        type:String
    },
    pendingForm: { // primeste id-ul form-ului in asteptare
        type:String
    },
});

const User = mongoose.model('User', userSchema);
module.exports = User;

const router = require('express').Router();
let Form = require('../models/form.model');
let User = require('../models/user.model');
let Route = require('../models/route.model');
let amqp = require('amqplib/callback_api');

router.route('/').get((req, res) => {
    Form.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err));
});

/* 
  register a new form
  FIXED
*/
router.route('/addForm').post((req, res) => {
    var startPoint = req.body.startPoint;
    var finishPoint = req.body.finishPoint;
    var passenger = req.body.passenger;
    var numberPeople = req.body.numberPeople;

    var newForm = {
        "startPoint": startPoint,
        "finishPoint" : finishPoint,
        "passenger": passenger,
        "numberPeople": numberPeople};

    Form.bulkWrite([
        {insertOne: {document: newForm}}])
      .then(form => {
        res.json(form);
        var formId = form.insertedIds[0];
        var userQuery = {"username": passenger};
        var toUpdate = {$set: {"pendingForm" : formId}};
          
        User.updateOne(userQuery, toUpdate)
            .then(form => {console.log(form)})
            .catch(err => {console.log(err)});
      })
      .catch(err => res.status(400).json('Error: ' + err));

});

let addToMessageQueue = (msg) => {
  amqp.connect('amqp://rabbitmq', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'mailing';

        channel.assertQueue(queue, {
            durable: false
        });
        channel.sendToQueue(queue, Buffer.from(msg));

        console.log(" [x] Sent %s", msg);
        
        setTimeout(function() {
          connection.close();
        }, 500);
    });
  });
};

// a driver accepts one form
// TODO: the notification thingy
// FIXED
router.route('/acceptFormDriver/:username').post((req, res) => {
  var driverUsername = req.params.username;
  var formID = req.body.formId;
  var queryForm = {"_id" : formID};

  Form.findOne(queryForm)
  .then(form => {
    const newDrivers = form.drivers;
    newDrivers.push(driverUsername);
    var toUpdate = {$set : {drivers: newDrivers}};

    Form.updateOne(queryForm, toUpdate)
          .then(form => {
            res.json(form);
            var toUpdateUser = {$set : {pendingForm: formID}};
            var queryUser = {"username" : driverUsername};

            User.updateOne(queryUser, toUpdateUser)
                .then(user =>{console.log("Updated the driver");})
                .catch(error => {console.log(error);});
          })
          .catch(err => {console.log(err);})
    
    var userQuery = {"username" : form.passenger};
    User.findOne(userQuery)
          .then(user => {
            addToMessageQueue(user.email);
          })
          .catch(err => {console.log(err);});

  })
  .catch(err => res.status(400).json('Error: ' + err));
      
});

/* 
    get all the drivers that accepted one form
*/
router.route('/getDrivers').get((req, res) => {
  var formID = req.body.formID;
  var queryForm = {"_id" : formID};

  Form.findOne(queryForm)
      .then(form => 
        res.json(form.drivers))
      .catch(err => res.status(400).json('Error: ' + err));
});

/*
    a passenger accepts a driver
    primeste ca parametru numele driver-ului ales
    primeste in body-ul request-ului id-ul formului cu care lucram
    FIXED
*/
router.route('/acceptFormPassenger/:username').post((req, res) => {
  var driverUsername = req.params.username;
  var formID = req.body.formId;
  var queryForm = {"_id" : formID};

  Form.findOne(queryForm)
  .then(form => {
    // cream o noua ruta cu informatiile din form
    var finishPoint = form.finishPoint;
    var startPoint = form.startPoint;
    var passenger = form.passenger;
    var newRoute = {  "startPoint": startPoint,
                      "finishPoint": finishPoint,
                      "driver": driverUsername,
                      "passenger": passenger};

    // adaugam ruta noua
    Route.bulkWrite([
      { insertOne: {document: newRoute}}])
        .then(route => {
          // update the active routes of the passanger and driver
          // remove their pending forms :)
          var routeId = route.insertedIds[0];
        
          var toUpdate = {$set : {  activeRoute : routeId,
                                    pendingForm : ""}};
          var userQueryDriver = {"username": driverUsername};
          var userQueryPassenger = {"username": passenger};

          User.updateOne(userQueryDriver, toUpdate)
              .then(user => {console.log(user)})
              .catch(err => {console.log(err)});
          User.updateOne(userQueryPassenger, toUpdate)
              .then(user => {console.log(user)})
              .catch(err => {console.log(err)});
        })
        .catch(err => {console.log(err)});
        res.json(form)
    })
  .catch(err => res.status(400).json('Error: ' + err));

  Form.deleteOne(queryForm)
      .then(function(){console.log("Deleted the form");})
      .catch(function(error){console.log(error);});

  var userQuery = {"username" : driverUsername};
  User.findOne(userQuery)
        .then(user => {
          addToMessageQueue(user.email);
        })
        .catch(err => {console.log(err);});
});

router.route('/testQueue').get((req, res) => {
  addToMessageQueue("Hello!!");
  res.status(200).send();
});

module.exports = router;